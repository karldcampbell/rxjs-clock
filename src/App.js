import React, { Component } from 'react';
import convert from 'color-convert';
import { tick$ } from './clock'
import './App.css';

const pad = (n) => String(n).padStart(2, '0');

const getHsl = (time) => {
    let hue = (time.min * 60 + time.sec) / 10;
    return [hue, 80, 40];
}
class App extends Component {
    state = {time: tick$.getValue()};
    
    constructor(props) {
        super(props);
        tick$.subscribe( _ => this.setState({time: _}));
    }
    
    renderTime(time) {
      return `${pad(time.hours)} : ${pad(time.min)} : ${pad(time.sec)}`
    };
    
  render() {  
    let hsl = getHsl(this.state.time);
    let bgColor = `#${convert.hsl.hex(hsl)}`;
    return (
      <div className="App" style={{backgroundColor: bgColor}}>
        <p className="time">{ this.renderTime(this.state.time) }</p>
        <p>hsl( {hsl[0].toFixed(1)},  {hsl[1]},  {hsl[2]}) </p>
        <p>{bgColor}</p>
      </div>);
  }
}

export default App;
