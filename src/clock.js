import { interval, BehaviorSubject } from 'rxjs'
import { map } from 'rxjs/operators'


const generateTicks = () => {
    const s = new BehaviorSubject(convDate(new Date()));
    
    interval(1000)
        .pipe( map(() => convDate(new Date())))
        .subscribe(s);
    
    return s;
}

export const tick$ = generateTicks();

function convDate(date) {
    let hours = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();
      
    return {hours, min, sec};
}
